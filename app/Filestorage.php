<?php namespace App;

/*
|--------------------------------------------------------------------------
| Filestorage (25.3.2015/J. Ruuska)
| Store files to location, use ahjocontract.json 'storage_dir' -parameter	
|--------------------------------------------------------------------------
*/

use Log;
use Illuminate\Support\Facades\File;
use App\Models\Param;
use App\Models\Cfile;
use Debugbar;

class Filestorage {
	
	public function __construct()
	{
		 
	}
	
	public static function path() {
		$params = new Param();
		$storage_dir = storage_path() . $params->storage();
	
		return $storage_dir;
	}
	
	public static function store($file , $contract_id) {
		
		$params = new Param();
		$storage_dir = storage_path() . $params->storage();

		//check directory
		if (!File::exists($storage_dir)) {
			File::makeDirectory($storage_dir);
			Log::info('Contract Directory created:' . $storage_dir . '!!');
		} else {
			//Log::info('Contract directory already exits:' . $storage_dir . '!!');
		}
		
		//create files-entry
		$cfile = new Cfile();
		$cfile->contract_id = $contract_id;
		$cfile->filename = '9999999' . $file->getClientOriginalName();
		$cfile->org_filename = $file->getClientOriginalName();
		$cfile->save();
		$cfile->filename = $cfile->id . '_' . $cfile->org_filename;
		$cfile->save();
		
		//store file: id + name
		$file->move($storage_dir, $cfile->filename);
		Log::info('Contract file stored:' . $cfile->filename);
		Debugbar::info('Contract file stored info:' . $cfile->filename);
	}
	
	public static function delete($id) {
		$params = new Param();
		$storage_dir = storage_path() . $params->storage();
		
		$cfile = Cfile::find($id);
		File::delete($storage_dir . '/' . $cfile->filename);
		Log::info('File removed:' .  $cfile->org_filename . ' [Contract_id:' .$cfile->contract_id . ']');
		$cfile->delete();
	}
}
