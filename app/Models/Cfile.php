<?php namespace App\Models;


class Cfile extends \Eloquent {
    
    protected $table = 'files';
    
    protected $fillable = array('*');
    
    // File __belongs_to__ Contract
    public function contract()
    {   
        return Contract::find($this->contract_id); 
    }

}
