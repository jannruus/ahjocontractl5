<?php

use App\Filestorage;

class Contract extends Eloquent {
    
    protected $table = 'contracts';
    
    protected $fillable = array('*');
    
    //Event handler for Contract
    public static function boot()
    {
    	parent::boot();
    
    	// Attach event handler, cascade on deleting of the contract (cascade not on table!)
    	Contract::deleting(function($contract)
    	{
    		// Delete all payments that belong to this contract
    		foreach ($contract->payments as $payment) {
    			$payment->delete();
    		}
    		// Delete all files that belong to this contract
    		foreach ($contract->files as $file) {
    			Filestorage::delete($file->id);
    		}
    	});
    }
    
    // Contract __belongs_to__ Vendor
    public function vendor()
    {
       return $this->belonsTo('Vendor', 'vendor_id', 'id'); 
 
    }
    
    public function vendorname()
    {
    	return Vendor::find($this->vendor_id);
    }
    
    public function typename()
    {
        return Contracttype::find($this->type)->name;
    }
    
    public function payments()
    {
    	return $this->hasMany('Payment', 'contract_id');
    }
    
    public function files()
    {
    	return $this->hasMany('App\Models\Cfile', 'contract_id');
    }
    
    //laravel Model scopes 
    public function scopeFilter($query, $type, $keyword, $order) 
    {
    	return $query->whereIn('type', $type)->where('name', 'LIKE', '%'.$keyword.'%')->orderBy('name', $order);
    }
    
    public function scopeFilterVendor($query, $vendor_id)
    {
    	return $query->where('vendor_id', $vendor_id);
    }
    
    public function scopeFilterSortVendor($query, $type, $keyword, $order)
    {
    	return $query = Contract::join('vendors', 'contracts.vendor_id','=','vendors.id')
    		->select('contracts.*')
    		->whereIn('contracts.type',$type)
    		->where('contracts.name', 'LIKE', '%'.$keyword.'%')
    		->orderBy('vendors.name', $order); 	// avoid fetching anything from joined vendors table
    }

}
