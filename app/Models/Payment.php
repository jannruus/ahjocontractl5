<?php

class Payment extends Eloquent {
    
    protected $table = 'payments';
    
    protected $fillable = array('*');
    
    // Payment __belongs_to__ Contract
    public function contract()
    {   
        return Contract::find($this->contract_id); 
    }
    
    public function typename()
    {
        return Paymenttype::find($this->type)->name;
    }
    

}
