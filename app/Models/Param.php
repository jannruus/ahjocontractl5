<?php namespace App\Models;
use Illuminate\Support\Facades\Storage;

// Parameters - read from ahjocontract.json file to PHP array
//

class Param {
	protected $params = null;

	public function __construct()
	{
	//	$params_file = storage_path() . '/ahjocontract.json';
	$params_file = 'ahjocontract.json';

	if (Storage::exists($params_file)) {
			$this->params = json_decode(Storage::get($params_file), true);
		} else {
			echo ($params_file . " parametritiedostoa ei löydy");
		}
	}

    public function organisation()
    {
        return $this->params['organisation'];
    }

    public function description()
    {
    	return $this->params['description'];
    }
    public function version()
    {
    	return $this->params['version'];
    }
    public function storage()
    {
    	return $this->params['storage_dir'];
    }
    public function aws()
    {
    	return $this->params['aws'];
    }
}
