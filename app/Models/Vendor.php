<?php

class Vendor extends Eloquent {
        
    protected $table = 'vendors';    
    
    protected $fillable = array('*');
    
    // Vendor __has_many__ Contract
    public function contracts()
    {
        return $this->hasMany('Contract');
    }
    
    public static function boot()
    {
    	parent::boot();
    
    	// Attach event handler, on deleting of the vendor
    	Vendor::deleting(function($vendor)
    	{
    		// Delete all contracts that belong to this vendor
    		foreach ($vendor->contracts as $contract) {
    			$contract->delete();
    		}
    	});
    }
    
}
