<?php

class Contracttype extends Eloquent {
    
    protected $table = 'contract_types';
    
    protected $fillable = array('*');
    public $timestamps = false;
    
        // Vendor __has_many__ Contract
    public function typename()
    {
        return $this->name;
    }
}
