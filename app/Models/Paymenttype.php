<?php

class Paymenttype extends Eloquent {
    
    protected $table = 'payment_types';
    
    protected $fillable = array('*');
    
    public $timestamps = false;
}
