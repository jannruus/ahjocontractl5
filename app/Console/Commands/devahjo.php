<?php namespace App\Console\Commands;
// testikomento laravel koodin pätkien suorittamiseen
// suoritus: php artisan devahjo
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\Param;
use App\Filestorage;
use URL;

class devahjo extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'devahjo';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Devaajan testipenkki';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//tässä on koodi joka suoritetaan
		$this->info('* devahjo - devaajan testikomento tool *');
		$this->info('Max file size of Server:' . ini_get('upload_max_filesize'));
		$this->info('AppPath '. app_path());
		$this->info('PublicPath '. public_path());
		$this->info('BasePath '. base_path());
		$this->info('StoragePath '. storage_path());
		$this->info( 'url: ' . url('/'));
		$this->info('* end *');

		//check the options
		$opt1 = $this->option('opt1');
		$this->line(" == Selection for the command: opt1 is {$opt1}  (php artisan devahjo --opt1=t) ==");

		if ($opt1 == "t") {

			$jrfile= 'ahjocontract.json';
			if (Storage::disk('s3')->exists($jrfile)) {
				$this->info($jrfile . '* on olemassa *');
			} else {
				$this->info($jrfile . '* ei ole olemassa *');
			}

			$files = Storage::disk('local')->allDirectories('');
				$this->info('* files *' . implode("|",$files));

		} else {

			//default
			if (!Storage::disk('s3')->exists(storage_path() . '/contracts')) {
				Storage::disk('s3')->makeDirectory('/contracts');
				Log::info('directory created:' . storage_path() . '\contracts. Defined in filesystems.php');
				$this->info(storage_path() . '/contracts' . '* hakemisto luotiin *');
			} else {
				Log::info('directory already exits:' . storage_path() . '/contracts. Defined in filesystems.php');
				$this->info(storage_path() . '/contracts' . '* hakemisto oli olemassa *');
			}

			//File operation test
			$testifile = storage_path() . '\contracts\15_IT2010_EJT.pdf';
			$this->info('local:' . Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix());
			$this->info('testifile:' . $testifile);
			$this->info('storage_path:' . storage_path());

			if (Storage::disk('local')->exists($testifile)) {
				Storage::disk('s3')->put('contracts/IT2010_EJT.pdf',  File::get($testifile));
				Log::info('file copied:' . $testifile .' to Aws S3');
				$this->info($testifile . '* tiedosto siirrettiin Aws S3 *');
			} else {
				Log::info('file doesnt exits:' . $testifile);
				$this->info($testifile . '* tiedostoa ei löydy *');
			}

		  //$params_file = storage_path() . '/ahjocontract.json';
			$params = null;
			$this->params = json_decode(Storage::get('ahjocontract.json'), true);
      $this->info('Parametri/organisation:' . $this->params['organisation']);

		} //end else
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

	/**protected function getArguments()
	{
		return [
			['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	} */

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			['opt1', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
