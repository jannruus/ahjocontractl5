<?php

/*
|--------------------------------------------------------------------------
| Application Routes - AhjoContracts
| Created: 9.6.2014
| Laravel 5 update: 14.3.2015
| Laravel 5.2 update: 22.6.2017
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
|
*/

Route::get('/' , 'App\Http\Controllers\ContractController@getIndex');
Route::get('/info', 'App\Http\Controllers\InfoController@index');
Route::get('/download/{cfile}', 'App\Http\Controllers\HomeController@getDownload');
Route::get('/report', 'App\Http\Controllers\ReportController@rcontract');
Route::get('/report/pdf', 'App\Http\Controllers\ReportController@rcontractpdf');
Route::get('/aws', 'App\Http\Controllers\AwsController@index');
Route::get('/contract/type/{type}', 'App\Http\Controllers\ContractController@getType');


// User RESTful controller
Route::controller('user', 'App\Http\Controllers\UserController');
Route::controller('contract', 'App\Http\Controllers\ContractController');
Route::controller('vendor', 'App\Http\Controllers\VendorController');
//Route::controller('password', 'App\Http\Controllers\RemindersController');
Route::controller('auth', 'App\Http\Controllers\Auth\AuthController');
Route::controller('password', 'App\Http\Controllers\Auth\PasswordController');

  
// This will check for all POST, PUT or DELETE request and if so, it will automatically use the already existing CSRF filter in Laravel.
//Route::when('*', 'csrf', array('post', 'put', 'delete'));
