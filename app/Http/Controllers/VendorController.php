<?php namespace App\Http\Controllers; 

/* VendorController 9.6.2014 (laravel5 update 19.3.2015)
*  RESTful controller - function name: HTTP verb + URI:
*  putStore = update
*  postStore = create 
*/ 
use View;
use Auth;
use Session;
use Redirect;
use Input;
use Vendor;
use Validator;
use DB;

class VendorController extends Controller {
    protected $layout = "layout";
    
    public function __construct()
    {
        //functions require authentication:    
        $this->middleware('auth');
    }
       
    public function getIndex() {
        //users list
        $vendors = Vendor::orderBy('name', 'asc')->paginate(25);
        return view('vendor.list')
        	->with('vendors', $vendors)
            ->with('keyword' , '');  //Search keyword
    }
    
    public function getNew() {
        $vendor = new Vendor;	//empty object in create  
    	return view('vendor.new')
    		->with('vendor', $vendor)
          	->with('method', 'post');  //post=create
    }
        
    
    public function getUpdate($id) {
        //update vendor view
        $vendor = Vendor::find($id);
        return view('vendor.new')
        	->with('vendor', $vendor)
        	->with('method', 'put');
    }
    
    private function formVendor(&$vendor) {
            $vendor->name = Input::get('name');
            $vendor->address1 = Input::get('address1');
            $vendor->address2 = Input::get('address2');
            $vendor->zipcode = Input::get('zipcode');
            $vendor->city = Input::get('city');
            $vendor->country = Input::get('country');
            $vendor->business_id = Input::get('business_id');
            $vendor->contact_name = Input::get('contact_name');
            $vendor->contact_tel = Input::get('contact_tel');
            $vendor->contact_email = Input::get('contact_email');
            $vendor->comment = Input::get('comment');
            $vendor->save();
    }
    
    
    //Create Vendor
    public function postStore() 
    {
 
       $rules = array(
       		'name'=>'required|unique:vendors|min:2',
           	'business_id'=>'required|unique:vendors|min:5', 
           	'contact_email'=>'email',
           	'contact_name'=>'required',
        ); 
                
        $validator = Validator::make(Input::all(), $rules);
 
        if ($validator->passes()) {
        // validation has passed, save vendor in DB
            $vendor = new Vendor;
            $this->formVendor($vendor);
            Session::flash('message', $vendor->name . ' lisätty');
            return Redirect::to('vendor/update/'.$vendor->id)
    		  	->withErrors($validator)
    			->withInput();
        } else {
        // validation has failed, display error messages
        	return Redirect::to('vendor/new')->with('error', 'Korjaa virheet ')
            	->withErrors($validator)
            	->withInput();
        }
        
    }
    
    //Update Vendor
    public function putStore($id)
    {
    	//dd('put storessa1');
    	$rules = array(
    		'name'=>'required|min:2',
    		'contact_email'=>'email',
    		'contact_name'=>'required',
    	);
		
    	$validator = Validator::make(Input::all(), $rules);
    
    	if ($validator->passes()) {
    		// validation has passed, save user in DB
    		$vendor = Vendor::find($id);
    		$this->formVendor($vendor);
    		//$this->getUpdate($vendor->id);
    		Session::flash('message', $vendor->name . ' päivitetty');
    		return Redirect::to('vendor/update/'.$vendor->id)
    		  	->withErrors($validator)
    			->withInput();
    	} else {
    		// validation has failed, display error messages
    		
    		return Redirect::to('vendor/update/'.Input::get('id'))->with('error', 'Korjaa virheet')
    			->withErrors($validator)
    			->withInput();
    	}
    }
    
    public function getDelete($id) {
    
    	// delete
    	$vendor = Vendor::find($id);
    	$vendor->delete();
    
    	// redirect
    	Session::flash('message', 'Toimittaja poistettu: ' . $vendor->name);
    	return Redirect::to('/vendor');
    }

    
    //search - vendors
    public function getSearch() {
         $keyword = Input::get('keyword');
         
        //new query
        $query = DB::table('vendors')->where('name', 'LIKE', '%'.$keyword.'%')->orderBy('name', 'asc');
   
        $vendors = $query->paginate(25); 

        return view('vendor.list')
            ->with('vendors', $vendors)
            ->withKeyword($keyword);  //Search keyword
    }
    
    // ************************************
    // SHOW Vendor INFO
    public function getShow($id) {
        $vendor = Vendor::find($id);
    return $vendor->name;
    }
    
    // SHOW Vendor INFO json-response
    public function getShowjson($id) {
        $vendor = Vendor::find($id);
    return $vendor->toJson();
    }
    
}