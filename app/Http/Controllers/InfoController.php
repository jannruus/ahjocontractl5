<?php namespace App\Http\Controllers;
use View;
use Auth;
use App\Models\Param;

class InfoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Info Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "info page" for the application.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$params = new Param();
		return view('info')
			->with('params', $params);
	}

}
