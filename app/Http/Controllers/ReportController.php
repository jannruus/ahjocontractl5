<?php namespace App\Http\Controllers;

use App, View, Auth, Input, Response, Debugbar, PDF;
use Contract;

class ReportController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Report Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's reports for users that
	| are authenticated. 
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the contract selection to the user.
	 *
	 * @return Response
	 */
	public function rcontract()
	{
		//Debugbar::info('Tultiin rcontract -metodiin');
		return view('report.rcontract');
	}
	
	// pdf-report
	public function rcontractpdf()
	{
		$vendor_id = Input::get('vendor_id');	//get selected vendor and pass it to report blade
		$data = array('vendor_id'=>$vendor_id);  //$vendor_id variable is the name in blade
		
		//$pdf = App::make('dompdf'); //Note: in 0.6.x this will be 'dompdf.wrapper'
		//$pdf->loadHTML('<h1>Testi raportti sopimuksista</h1>');
		$pdf = PDF::loadView('report.rcontractpdf', $data);
		return $pdf->stream();
	}

}
