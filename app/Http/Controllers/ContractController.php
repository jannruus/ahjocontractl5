<?php namespace App\Http\Controllers;

/* ContractController 13.6.2014
*  RESTful controller - function name: HTTP verb + URI
*/

/*  15.3.2015/JR
class ContractController extends BaseController {
*/

use View;
use Auth;
use Input;
use Redirect;
use Contract;
use Payment;
use App\Models\Cfile;
use App\Filestorage;
use Session;
use Validator;
use Debugbar;
use Log;

class ContractController extends Controller {
    protected $layout = "layout";

    public function __construct()
    {
        //functions require authentication:
        $this->middleware('auth');
        
        //after sign-in, we need to ensure the contract type for listview, stored in Session
        if (!Session::has('type')) {
        	Session::put('type','Z');	//N-Notes, S-NDAs, all others are conracts (table contract_types)
        }
        
        //Sorting order of contracts, stored in Session
        if(Input::has('sortby')) {
        	Session::put('sortby',Input::get('sortby'));  	//'name', 'vendor_id'
        	Session::put('order',Input::get('order'));		//'asc' , 'desc'
        } elseif (!Session::has('sortby')) {
        	Session::put('sortby','name');  	//default 'name'
        	Session::put('order', 'asc');		//default 'asc'
        } else {
        	//don't do nothing, everything is ok
        }
        	
        Debugbar::info('[ContractController.php] Contract type in Session (N-Notes, Z-Contracts(default):' . Session::get('type'));
        
    }

    
    //contract list
    public function getIndex() {

        //Order and search filters of the contact list stored in the Session
    	$sortby = Session::get('sortby');
        $order = Session::get('order');
        $searchtype = Session::get('searchtype');  	//search:Contract Type
        $vendor_id = Session::get('vendor_id');		//search:Vendor
        $keyword = Session::get('keyword');			//search:string in contract name
        
        //make contract type selection
        if (Session::get('type') == 'Z') {			//all contract types except Notes
        	if ($searchtype<>'') {					//filter specific contract types
        		$selectType = array($searchtype);
        	} else {
        		$selectType = array('L' ,'P' , 'K','F' ,'S' ,'W' ,'M');		//do not show N-Notes
        	}
        } else {
        	$selectType = array('N');		//show only N-Notes
        }        

        //run query

        //Sort by vedor_name using table join
      	if ($sortby == "vendor_id") {
      		if ($vendor_id == '') {	
      			$contracts = Contract::filterSortVendor($selectType, $keyword, $order)->paginate(25);  //scopeFilterSortVendor
      		} else {
      			$contracts = Contract::filterSortVendor($selectType, $keyword, $order)->filterVendor($vendor_id)->paginate(25);  //Vendor id given in searchbox
      		}
      	} else {
             if ($vendor_id == '') {
             	$contracts = Contract::filter($selectType, $keyword, $order)->paginate(25);		//utilise Model scope
             } else {
             	$contracts = Contract::filter($selectType, $keyword, $order)->filterVendor($vendor_id)->paginate(25);  //utilise Model scope chain
             }
    	}

        return view('contract.list')->with('contracts', $contracts);
    }

    public function getType($type) {
    	Session::put('type',$type);
    	Debugbar::info('Valittu tyypiksi:' . Session::get('type'));
    	return Redirect::to('/');
    }

    public function getNew() {
          return view('contract.new')
            ->with('mode', 'C');    //C=Create
    }


    public function getUpdate($id) {
        //update contract view
        $contract = Contract::find($id);
        $payments = $contract->payments()->get();
        $files = $contract->files()->get();
        return view('contract.new')
        ->with('contract', $contract)
        ->with('payments', $payments)
        ->with('files', $files)
        ->with('mode', 'U');
    }

    private function formContract(&$contract) {

            $contract->name = Input::get('name');
            $contract->type = Input::get('type');
            $contract->vendor_id = Input::get('vendor');
            $contract->start_date = date("Y-m-d", strtotime(Input::get('start_date')));
            //empty date check
            if(strtotime(Input::get('end_date'))!=''){
            	$contract->end_date = date("Y-m-d", strtotime(Input::get('end_date')));
            }else{
            	$contract->end_date = '';
            }
            $contract->countinuous = Input::get('countinuous');
            $contract->termination_period = Input::get('termination_period');
            $contract->status = Input::get('status');
            $contract->comment = Input::get('comment');
            $contract->filelink = Input::get('filelink');
            $contract->user_id = Auth::user()->id;
            if ($contract->termination_period == '') { $contract->termination_period = 0; }
            if ($contract->countinuous == null) { $contract->countinuous = 0; }

           //debug
            if(strtotime(Input::get('end_date'))!=''){
            	 $contract->end_date = date("Y-m-d", strtotime(Input::get('end_date')));
            }else{
            	$contract->end_date = '';
            }

            $contract->save();

            //store multiple files
            if (Input::hasFile('contract')) {
            	$files = Input::file('contract');
            	foreach($files as $file) {
            		Filestorage::store($file, $contract->id);
            	}
            }

            /**
            if (Input::hasFile('contract')) {
            	$file = Input::file('contract');
            	Filestorage::store($file, $contract->id);
            	Session::flash('success', 'Upload successfully');
            } else {
            	Session::flash('error', 'uploaded file is not valid');
            	Log::info('Uploaded contract file failed:' . $storage_dir . '!!');
            }
            */
    }

    //Create/Update Contract
    public function postStore()
    {
    	if (Input::get('id') == '0') {      //new-create
            $rules = array(
                'name'=>'required|min:6',
                'start_date'=>'required|date',
                'end_date'=>'date',
                'termination_period'=>'numeric',
                );
        } else {                            //update
            $rules = array(
                'name'=>'required|min:6',
                'start_date'=>'required|date',
                'end_date'=>'date',
                'termination_period'=>'numeric',
                );
         }

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->passes()) {
        // validation has passed, save user in DB
            if (Input::get('id') == '0') {
                $contract = new Contract;
                Session::flash('message', 'Sopimus luotu');
            } else {
                $contract = Contract::find(Input::get('id'));
                Session::flash('message', $contract->name . ' päivitetty');
            }

            $this->formContract($contract);

            // return Redirect::to('/')->with('message', $contract->name . ' luotu');
            //$this->getUpdate($contract->id);

            //Session::flash('message', $contract->name . ' käsitelty');
            return Redirect::to('contract/update/'.$contract->id)
            	//	->withErrors($validator)
            		->with('mode', 'U');
            	//	->withInput();
        } else {
        // validation has failed, display error messages
        	if (Input::get('id') == '0') {
            	return Redirect::to('contract/new')->with('error', 'Korjaa virheet ')
            	->withErrors($validator)
            	->withInput();
        	} else {
        		return Redirect::to('contract/update/'.Input::get('id') )->with('error', 'Korjaa virheet ')
        		->withErrors($validator)
        		->withInput();
        	}
        }
    }

	// Delete Contract by URL
    public function getDelete($id) {

    	// delete
    	$contract = Contract::find($id);
    	$contract->delete();

    	// redirect
    	Session::flash('message', 'Sopimus poistettu: ' . $contract->name);
    	return Redirect::to('/');
    }
    // Delete Contract by form Put
    public function putDelete($id) {

        // delete
        $contract = Contract::find($id);
        $contract->delete();

        // redirect
        Session::flash('message', 'sopimus poistettu: ' . $contract->name);
        return Redirect::to('/');
    }

    //search - contracts
    public function getSearch() {
         
    	$keyword = Input::get('keyword');
        $searchtype = Input::get('searchtype');
        $vendor_id = Input::get('vendor_id');
   
        Session::put('searchtype', $searchtype);  //set for search box
        Session::put('vendor_id', $vendor_id);
        Session::put('keyword', $keyword);
        
        return Redirect::to('/');

    }
    
    //search - contracts clear session variables
    public function getClear() {
    	Session::put('searchtype', '');  //set for search box
    	Session::put('vendor_id', '');
    	Session::put('keyword', '');
    	
    	Session::flash('message', 'Hakutekijät tyhjennetty');
    	return Redirect::to('/');
    	
    }

    //get payment details
    public function getPayment($id) {
    	$payment = Payment::find($id);
    	return $payment->toJson();

    }

    //update payment details
    public function postPaymentstore() {

    	if (Input::get('pid') == '') {      //new-create
    		$payment = new Payment;
    		$payment->type = Input::get('type');
    		$payment->value = Input::get('value');
    		$payment->comment = Input::get('comment');
    		$payment->contract_id = Input::get('cid');
    	} else {							//update
    		$payment = Payment::find(Input::get('pid'));
    		$payment->type = Input::get('type');
    		$payment->value = Input::get('value');
    		$payment->comment = Input::get('comment');
    	}

    	$payment->save();

    	//$this->getUpdate($payment->contract_id);
    	// redirect
        Session::flash('message', "Maksu lisätty: " . $payment->value . ' ' . $payment->comment );
        return Redirect::to('contract/update/'.$payment->contract_id);
            	//->withInput(Input::except('comment'));    //payment has also comment

    }

    public function getPaymentdelete($id) {

    	// delete payments-entry
    	$payment = Payment::find($id);
    	$cid = $payment->contract_id;
    	$payment->delete();

    	// redirect
            Session::flash('message', "Maksu poistettu: " . $payment->value . ' ' . $payment->comment );
        
       return Redirect::to('contract/update/'.$payment->contract_id);
    }

    public function getFiledelete($id) {

    	// delete files-entry and the actual file
    	$cfile = Cfile::find($id);
    	Filestorage::delete($id);

    	// redirect
    	Session::flash('message', "Liite poistettu: " . $cfile->filename);
    	return Redirect::to('contract/update/'.$cfile->contract_id)
    		->withInput();
    }

}
