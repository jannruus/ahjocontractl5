<?php namespace App\Http\Controllers; 

/* Janne luonut 3.5.2014
* RESTful controller - function name: HTTP verb + URI
* - example: /user/create comes to this controller with form POST, function name is postCreate
* - example: /user/register comes to this controller with http-request GET, function name is getRegister    
*/ 

use View;
use Auth;
use Hash;
use Session;
use Redirect;
use Input;
use User;
use Validator;


class UserController extends Controller {
    protected $layout = "layout";
    
    public function __construct()
    {
        //functions require authentication:    
       // $this->beforeFilter('auth', array('only' => array('getRegister', 'postCreate', 'getList')));
    	//$this->middleware('auth');
    }
    
    public function getList() {
        //users list
    	//$this->middleware('auth');
    	if (Auth::check()) {
    		$users = User::orderBy('username', 'asc')->paginate(14);
        	return view('user.list')->with('users', $users);
    	} else {
    		Session::flash('message', 'Kirjaudu ensin sisään! ');
    		return Redirect::to('auth/login');
    	}
  
    }
        
    public function getRegister() {
        //new user view
    	if (Auth::check()) {
    	if(Auth::user()->is_admin ) {
        	return view('user.register');
    	} else {
    		Session::flash('message', Auth::user()->username . ': Ei oikeuksia luoda tunnuksia ');
    		return Redirect::to('user/list'); 
    	}
    	} else {
    		Session::flash('message', 'Kirjaudu ensin sisään! ');
    		return Redirect::to('auth/login');
    	}
    }
    public function getUpdate($id) {
        //update user view
    	//dd('Auth User:' . Auth::user()->is_admin); // returns User object
    	if(Auth::user()->is_admin) {
        	$user = User::find($id);
        	return view('user.edit', array('user' => $user));
    	} else{
    		Session::flash('message', Auth::user()->username . ': Ei oikeuksia muokata tunnuksia ');
    		return Redirect::to('user/list');
    	}
    }
    
    /* public function getLogin() {
        return view('auth.login');
    }
    */
    
    public function getLogout() {
        Auth::logout();
        Session::flush();    
        return Redirect::to('auth/login');
    }
    
    //Create User
    public function postCreate() 
    {
        $rules = array(
            'username'=>'required|unique:users|alpha|min:2',
            'email'=>'required|email',
            'password'=>'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'=>'required|alpha_num|between:6,12'
         );
                
        $validator = Validator::make(Input::all(), $rules);
 
        if ($validator->passes()) {
        // validation has passed, save user in DB
            $user = new User;
            $user->username = Input::get('username');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->is_admin = Input::get('is_admin');
            if ($user->is_admin == null) {
            	$user->is_admin = false;
            }
            $user->save();
 
        return Redirect::to('user/list')->with('message', $user->username . ' käyttäjätunnus lisätty');
        } else {
        // validation has failed, display error messages    
          return Redirect::to('user/register')
          	->with('error', 'Korjaa virheet')
          	->withErrors($validator)
          	->withInput();
        }
    }
    
    //update User
    public function putUpdate($id) {
           
       if (Input::get('chg_pwd') == 'yes') {   //update also password
       
            $rules = array(
                'email'=>'required|email',
                'password'=>'required|alpha_num|between:6,12|confirmed',
                'password_confirmation'=>'required|alpha_num|between:6,12'
            );
         } else {                               //no password update
            $rules = array(    
                'email'=>'required|email'
            );
       };
                
        $validator = Validator::make(Input::all(), $rules);
 
        if ($validator->passes()) {
        // validation has passed, save user in DB
            $user = User::find($id);
            $user->email = Input::get('email');
            if (Input::get('chg_pwd') == 'yes') { 
                $user->password = Hash::make(Input::get('password'));
            };
            $user->is_admin = Input::get('is_admin');
            if ($user->is_admin == null) {
            	$user->is_admin = false;
            }
            
            $user->save();
            //return Redirect::to('user/list')->with('message', 'Käyttäjän ' . $user->username . ' tiedot päivitetty');
            //$this->getUpdate($user->id);
            Session::flash('message', $user->username . ' päivitetty');
            return Redirect::to('user/update/' . $id)
            	->withInput();
        
        } else {
        // validation has failed, display error messages    
           return Redirect::to('user/update/' . $id)
           	->with('error', 'Korjaa virheet ')
           	->withErrors($validator)
           	->withInput();
        }
        
    }
    
    public function postSignin() {
        
    if (Auth::attempt(array('username'=>Input::get('username'), 'password'=>Input::get('password')))) {
        return Redirect::to('contract')->with('message', 'Olet kirjautunut sisään tunnuksella ' . Input::get('username'));
        } else {
            return Redirect::to('auth/login')
            ->with('error', 'Käyttäjätunnus tai salasana väärin')
            ->withInput();
        }         
    }
    
    public function putDelete($id) {
        
        // delete
    	if(Auth::user()->is_admin) {
    		$user = User::find($id);
    		if(Auth::user()->username != $user->username ) {
        		$user->delete();
        		Session::flash('message', $user->username . ' Käyttäjätunnus poistettu!');
    		} else {
    			Session::flash('message', Auth::user()->username . ': Et voi poistaa omaa tunnustasi! ');
    		}	
    	} else {
    		Session::flash('message', Auth::user()->username . ': Ei oikeuksia poistaa tunnuksia ');
    	}

        // redirect
        return Redirect::to('user/list');     
    }
    
}