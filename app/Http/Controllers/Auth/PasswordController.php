<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use View;
use Input;
use Password;
use Hash;
use Redirect;
use Lang;
use Illuminate\Http\Request;

class PasswordController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Password Reset Controller
	|--------------------------------------------------------------------------
	|
	| This controller is responsible for handling password reset requests
	| and uses a simple trait to include this behavior. You're free to
	| explore this trait and override any methods you wish to tweak.
	|
	*/

	use ResetsPasswords;

	/**
	 * Create a new password controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\PasswordBroker  $passwords
	 * @return void
	 */
	public function __construct(Guard $auth, PasswordBroker $passwords)
	{
        $this->auth = $auth;
        $this->passwords = $passwords;
        $this->subject = 'AhjoContract -Your Password Reset Link'; //  < --JUST ADD THIS LINE
        $this->middleware('guest');
	}
	
	public function getRemind()
	{
		return View::make('password.remind');
	}
	
	public function postRemind(Request $request)
	{
		$this->validate($request, ['email' => 'required']);
	
		$response = $this->passwords->sendResetLink($request->only('email'), function($message)
		{
			$message->subject('AhjoContract - Salasanan nollaus');
			$message->from('janne.ruuska@hotmail.fi', 'AhjoContract1');

		});
	
		switch ($response)
		{
			case PasswordBroker::RESET_LINK_SENT:
				return redirect()->back()->with('info', trans($response));
	
			case PasswordBroker::INVALID_USER:
				return redirect()->back()->withErrors(['email' => trans($response)]);
		}
	}
	
	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);
	
		return View::make('password.reset')->with('token', $token);
	}
	
	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
				'email', 'password', 'password_confirmation', 'token'
		);
	
		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);
	
			$user->save();
		});
	
		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));
	
			case Password::PASSWORD_RESET:
				return Redirect::to('/');
		}
	}

}
