## AhjoContract (using Laravel PHP Framework)

[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

AhjoContract on sopimustenhallinta sovellus. Sovelluksella voidaan tallentaa sopimuksia perustietoineen sekä kokousmuistioita liitteineen. 


![AhjoContractpic1.png](https://bitbucket.org/repo/GE6qad/images/3519842110-AhjoContractpic1.png)

Demoon pääset tutustumaan tästä osoitteesta:

http://ahjocontractdemo.azurewebsites.net/index.php

Demo tunnus on *demo* ja salasana *demo01*

Voit ottaa tämän projektin git komennolla itsellesi. Jos haluat tulla mukaan jatkokehittämään sovellusta, ota yhteyttä, niin pystyt tuomaan muutoksesi osaksi master-haaraa.

## Asennus paikallisesti

Ennen sovelluksen asentamista, asenna PHP ohjelmisto (5.3 tai uudempi). Varmista, että seuraavat PHP-laajennokset on käytössä, eli kommentit poistettu (**php.ini**):

*extension=php_mbstring.dll
extension=php_fileinfo.dll
extension=php_openssl.dll
extension=php_pdo_sqlite.dll
max_file_uploads = 20*

Lataa tämä projekti koneellesi git:llä ja voit seurata samoja ohjeita kuin Azeru asennus. Eli varmista, että koneellasi on Composer apuohjelma. Aja komento composer install latauksen jälkeen juurihakemistossa (eli se, mistä löytyy composer.json -tiedosto). Tämän jälkeen voitkin jo käynnistää laraver palvelimen komennolla:
*php artisan serve* ja avaa selain localhost osoitteeseen. 

Perustietoa Laravel asennuksesta löytyy [Laravel]https://laravel.com/docs/5.1/installation kotisivuilta.

## Asennus Microsoftin Azure palvelimelle

Helpoin tapa asentaa sovellus itsellesi lienee Microsoftin Azure pilvipalveluympäristö. AhjoContract on normaali web-sovellus, joka toimii web-palvelimen päällä.

### Azure web-palvelun luominen (jos tiliä ei ole olemassa)
 - Ota Microsoftin Azure-palvelut käyttöösi osoitteessa https://portal.azure.com, tarvittaessa luo tili. Aluksi riittää web-sovelluspalvelutilaus, joka tehdään seuraavasti:
- tutustumiskäyttöön valitaan Pay-as-you -subscription, ja siitäkin Free-taso 
(https://azure.microsoft.com/en-us/free/)

![AhjoContractpic2.png](https://bitbucket.org/repo/GE6qad/images/2797457906-AhjoContractpic2.png)

### Web-palvelun luomisen jälkeen aloitetaan sovelluksen asennus. ###
* tilauksen jälkeen luodaan AppService-palvelu sovellukselle (AppService näyttö ja valitse +Add)
* valitse Web Apps (ei tarvita tietokantapalveluita)
* App name kenttään tulee url-osoitteen ensimmäinen nimi, esim "AhjoContractDemo". Muut valinnat eivät vaikuta sovelluksen toimintaan
* paina Create ja Azure ilmoittaa Deployment started -> Deployment succeeded. 
* uusi luomasi App Service tulee AppService listaan näkyviin, ja web-palvelu on käytössäsi 
* lisää apuohjelma Composer extension web-palvelun käyttöön (Laravel tarvitsee PHP asennuksiin)
* Development Tools->Extensions->Add->1.Choose Extension->Composer->OK

### AhjoContract sovelluksen asentaminen ###

Sovellus asennetaan Azureen suoraan tästä BitBucketin sovellusrepositorysta git-toiminnallisuuden avulla. Microsoftin perusohje löytyy täältä (Microsoftin perusohje löytyy täältä (http://blog.davidebbo.com/2013/04/publishing-to-azure-web-sites-from-any.html), mutta käydään se vaiheittan läpi:

* valitse AppService (tässä AhjoContractDemo)->App Deloyment->Deployment options->External Repository ja anna valinnat:
* Repository URL: https://jannruus@bitbucket.org/jannruus/ahjocontractl5.git
* Branch: master
* Repository Type: Git

Sovelluksen mukana ei tule Laravel-kirjastoja, joten ne täytyy asentaa erikseen:
* käynnistä konsoli: AhjoContractDemo->Development Tools:Console
* anna komento composer install (ks kuva)

![AhjoContractpic3.png](https://bitbucket.org/repo/GE6qad/images/2564791112-AhjoContractpic3.png)

Tietokannan luonti
* asennuspaketin mukana tulee kehitystietokanta, joka kopioidaan pohjaksi antamalla copy komento seuraavaksi:

![AhjoContractpic4.png](https://bitbucket.org/repo/GE6qad/images/1074332102-AhjoContractpic4.png)

### Web-palvelun muuttujien asetus ###

Lisää seuraavat ympäristömuuttujat:

* AhjoContractDemo->Settings:Application settings->App settings

- APP_KEY: p9N9kyfdIOWpkS55wwZDmkM1eYwv6WDY
- APP_DEBUG: false

![AhjoContractpic5.png](https://bitbucket.org/repo/GE6qad/images/1244942223-AhjoContractpic5.png)

* AhjoContractDemo->Settings:Application settings->Virtual Applications and Directories 

- muuta '/'  riville arvoksi 'site\wwwroot\public'

![AhjoContractpic6.png](https://bitbucket.org/repo/GE6qad/images/2251638029-AhjoContractpic6.png)

**Asennus on valmis, toimii osoitteessa** 

http://ahjocontractdemo.azurewebsites.net/index.php

Demo tunnus: demo/demo01 (tai pääkäyttäjä admin/adminadmin)

**Muuta**

Salasanan nollaus-toiminnon käyttöönotto vaatii postipalvelimen asetusten muuttamista ympäristötiedostossa .env. Muuttujat ovat MAIL_ nimisiä.

Päivitykset voit ladata sovellushakemistoosi tästä BitBucket Git-palvelimelta, [Download Repository](https://jannruus@bitbucket.org/jannruus/ahjocontractl5.git)

## Dokumentaatio

Tämä sivu toimii ohjelmiston dokumenttina.

**Varmistus**

Tietokanta on ./storage/ahjocontract.sqlite, varmista tämä säännöllisesti
Ladatut dokumentit tallentuvat kansioon ./storage/contracts, varmista hakemiston sisältö säännöllisesti

**Asetukset**

Asetuksia on tiedostossa ./storage/ahjocontract.json, muuta esim:
-> "organisation": "Oma Organisaation nimi"

### License

AhjoContract is built using Laravel 5 PHP framework. Developed by Janne Ruuska 2014-2016.

The AhjoContract is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)