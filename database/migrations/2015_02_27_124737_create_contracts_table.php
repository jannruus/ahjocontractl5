<?php namespace App\Database\migrations;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		      Schema::create('contracts', function($t) {
                $t->increments('id');
                $t->string('name', 100);
                $t->char('type', 2);
                $t->integer('vendor_id')->unsigned(); 
                $t->foreign('vendor_id')->references('id')->on('vendors');
                $t->date('start_date');
                $t->date('end_date')->nullable();;
                $t->tinyInteger('countinuous');
                $t->integer('termination_period')->unsigned()->nullable();;
                $t->string('comment', 300)->nullable();;
                $t->string('filelink', 200)->nullable();
                $t->integer('user_id')->unsigned();
                $t->timestamps();
               });
    
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('contracts');
	}
	

}
