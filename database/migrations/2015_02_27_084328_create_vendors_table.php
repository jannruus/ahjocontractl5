<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		        Schema::create('vendors', function($t) {
                $t->increments('id');
                $t->string('name', 100)->index();
                $t->string('address1', 64);
                $t->string('address2', 64);
                $t->string('zipcode', 12);
                $t->string('city', 48);
                $t->string('country', 48);
                $t->string('business_id', 16)->unique();
                $t->string('contact_name', 50);
                $t->string('contact_tel', 20);
                $t->string('contact_email', 64);
                $t->string('comment', 300);
                $t->timestamps();
               });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vendors');
	}

}
