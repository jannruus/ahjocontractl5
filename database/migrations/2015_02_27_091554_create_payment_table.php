<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentTable extends Migration {

	/**
	 * Run the migrations. php artisan migrate
	 *
	 * @return void
	 */
	public function up()
	{
		        Schema::create('payments', function($t) {
                $t->increments('id');
                $t->char('type', 2);
                $t->integer('contract_id')->unsigned();
                $t->foreign('contract_id')->references('id')->on('contracts');
                $t->decimal('value', 10,2);
                $t->string('comment', 100);
                $t->timestamps();
               });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('payments');
	}

}
