<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCascadeDeletePayments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		// add hard delete to payments table, not working in SQLITE3
		Schema::table('payments', function($t) {
			$t->dropForeign('contract_id');
			$t->foreign('contract_id')->references('id')->on('contracts')->onDelete('cascade');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::table('payments', function($t) {
			$t->dropForeign('contract_id');
		});

	}

}
