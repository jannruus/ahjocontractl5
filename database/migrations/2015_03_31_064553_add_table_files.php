<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableFiles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
			//
		Schema::create('files', function($t) {
			$t->increments('id');
			$t->integer('contract_id')->unsigned();
			$t->foreign('contract_id')->references('id')->on('contracts');
			$t->string('filename', 64)->unique();
			$t->string('org_filename', 64);
			$t->timestamps();
	});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('files');
	}

}
