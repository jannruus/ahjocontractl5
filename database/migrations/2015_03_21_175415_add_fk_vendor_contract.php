<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkVendorContract extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// add fk to contracts table
		Schema::table('vendors', function($t) {
			$t->foreign('id')->references('id')->on('contracts');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		$table->dropForeign('contracts_vendor_id_foreign');
	}

}
