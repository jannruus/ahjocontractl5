<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ContracttypesSeeder extends Seeder {

	/**
	 * Run the database seeds: php artisan db:seed --class=ContracttypesSeeder
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d H:i:s');

		Contracttype::create(
		array(
		'id'   => 'L',
		'name' => 'Lisenssi'
		));
		
		Contracttype::create(
		array(
		'id'   => 'M',
		'name' => 'Ylläpito'
		));
		
		Contracttype::create(
		array(
		'id'   => 'P',
		'name' => 'Projekti'
		));
		
		Contracttype::create(
		array(
		'id'   => 'K',
		'name' => 'Konsultti'
		));
		
		Contracttype::create(
		array(
		'id'   => 'F',
		'name' => 'Puitesopimus'
		));
		
		Contracttype::create(
		array(
		'id'   => 'S',
		'name' => 'Palvelusopimus'
		));
		
		Contracttype::create(
		array(
		'id'   => 'W',
		'name' => 'Työtilaus'
		));
		
		Contracttype::create(
		array(
		'id'   => 'N',
		'name' => 'Muistio'
		));

	}
}