<?php

class ContractSeeder extends Seeder {

	/**
	 * Run the database seeds: php artisan db:seed --class=ContractSeeder
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d');

		Contract::create(
		array(
			'name' => 'Test Contract',
			'type' => 'F',
			'vendor_id' => 1,
			'start_date' => $now,
			'end_date' => $now,
			'countinuous' => '0',
			'termination_period' => 3,
			'comment' => 'Seeded by ContractSeeder',
			'user_id' => 1
		));
		
	}
}