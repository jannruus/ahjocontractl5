<?php

class StatusSeeder extends Seeder {

	/**
	 * Run the database seeds: php artisan db:seed --class=StatusSeeder
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d H:i:s');

		Status::create(
		array(
		'id'   => 'V',
		'name' => 'Voimassa'
		));
		
		Status::create(
		array(
		'id'   => 'A',
		'name' => 'Ei voimassa'
		));
		
		Status::create(
		array(
		'id'   => 'D',
		'name' => 'Luonnos'
		));

	}
}