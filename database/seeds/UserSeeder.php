<?php

class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d H:i:s');

		User::create(
		array(
				'username'   => 'admin',
				'password'   => Hash::make('jataru65'),
				'email'   	=> 'janne.ruuska@ahjoapps.com',
				'is_admin'   => 1,
				'created_at' => $now,
				'updated_at' => $now
		));

		User::create(
		array(
				'username'   => 'janne',
				'password'   => Hash::make('jataru65'),
				'email'   => 'janne.ruuska@hotmail.fi',
				'is_admin'   => 1,
				'created_at' => $now,
				'updated_at' => $now
		));

		User::create(
		array(
				'username'   => 'demo',
				'password'          => Hash::make('demo01'),
				'email'   => 'janne.ruuska@outlook.com',
				'is_admin'   => 0,
				'created_at' => $now,
				'updated_at' => $now
		));
	}
}