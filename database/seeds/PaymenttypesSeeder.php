<?php

class PaymenttypesSeeder extends Seeder {

	/**
	 * Run the database seeds: php artisan db:seed --class=PaymenttypesSeeder
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d H:i:s');

		Paymenttype::create(
		array(
		'id'   => 'CV',
		'name' => 'Sopimuksen arvo'
		));
		
		Paymenttype::create(
		array(
		'id'   => 'L',
		'name' => 'Lisenssimaksu'
		));
	
		Paymenttype::create(
		array(
		'id'   => 'LY',
		'name' => 'Lisenssiylläpito'
		));
		
		Paymenttype::create(
		array(
		'id'   => 'MA',
		'name' => 'Ylläpitomaksu'
		));
		
		Paymenttype::create(
		array(
		'id'   => 'PV',
		'name' => 'Projektisopimuksen arvo'
		));
		
		Paymenttype::create(
		array(
		'id'   => 'HV',
		'name' => 'Tuntihinta'
		));

	}
}