<?php

class VendorSeeder extends Seeder {

	/**
	 * Run the database seeds: php artisan db:seed --class=VendorSeeder
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

	$now = date('Y-m-d H:i:s');

		Vendor::create(
		array(
			'name' => 'AhjoApps',
			'address1' => 'Juhonkuja 4',
			'address2' => '',
			'zipcode' => '00420',
			'city' => 'Kerava',
			'country' => 'Suomi',
			'business_id' => '123456789-0',
			'contact_name' => 'Janne Ruuska',
			'contact_tel' => '040-5211144',
			'contact_email' => 'janne.ruuska@ahjoapps.com',
			'comment' => 'Seeded by VendorSeeder',
		));
		
	}
}