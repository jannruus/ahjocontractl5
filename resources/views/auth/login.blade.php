@extends('layout')
  <!-- 16.3.2015/jr: l4 versio -->
@section('content')

  <div class="col-md-4">
  	<br><img src="{{asset('img/Ahjoappslogo.png')}}" alt="nologo"/><br>
    <br><br><div class="loginimg"> </div>
  </div>
  
<form class="form-horizontal" role="form" method="POST" action="{{ url('/user/signin') }}">
	<input type="hidden" name="_token" value="{{ csrf_token() }}">

    
    <h3 class="form-signin-heading col-md-2 col-md-offset-2">Kirjaudu</h3>
    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
        	<input type="text" class="form-control" placeholder="käyttäjätunnus" name="username">
        </div> 
    </div>

    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            <input type="password" class="form-control" placeholder="salasana" name="password">
        </div> 
    </div>
    <br>
    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            <button type="submit" class="btn btn-primary">Kirjaudu sisään</button>
        </div> 
    </div>
</form>
   
    @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-3 col-md-offset-6">
        	<div class="alert alert-danger">
           		<p>{{ Session::get('error' )}}</p>  
         	</div>
        </div>
      </row>
    @endif

@stop