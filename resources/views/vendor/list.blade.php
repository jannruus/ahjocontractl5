<!-- Vendor list view -->
@extends('layout')
@section('content')
@include('showvendor')

    <row>
      <div class="col-md-8 col-md-offset-2">
        <button type="button" class="btn btn-default btn-sm">
            <span class="glyphicon glyphicon-plus"></span> <a href="{{ URL::to('vendor/new') }}">Uusi Toimittaja</a>
        </button>
      </div>
    </row>
    <br><br>
    <row>

    	<div class="col-md-2">
    	<div class="panel panel-default bs-search">
        	<label for="haku" class="control-label input-sm">Nimihaku</label>
            	<form class="form-horizontal" role="form" method="GET" action="{{ url('/vendor/search') }}">
                    {!!Form::text('keyword', $keyword, array('class' => 'form-control input-sm', 'placeholder'=>'etsi nimellä'))!!}<br>
                    <button type="submit" class="btn btn-primary btn-sm">Etsi</button>
           		</form>
        </div>  </div>

    <!--  List section -->

        <div class="col-md-9">
        <div class="panel panel-primary">
                  <!--  <div class="panel panel-primary" style="width:800px"> -->
  <!-- Default panel contents -->
            <div class="panel-heading">Toimittajat</div>

    <!-- Table -->
            <table class="table-condensed table-striped table-hover input-sm">
            <tr>

                <th style="width:200px">Nimi</th>
                <th style="width:180px">Tunnus</th>
                <th style="width:100px">Sijainti</th>
                <th>Yhteyshenkilö</th>
                <th style="width:20px">Sopimus</th>
                <th style="width:45px">Action</th>
            </tr>
            @foreach($vendors as $vendor)
            <tr>

                <td><a class="btn btn-link btn-sm showvendor" data-target="#showvendor" data-id="{{$vendor->id}}" > {{ $vendor->name }}</a> <!-- link to Modal dialog showvendor.php -->
                <td>{{ $vendor->business_id }}</td>
                <td>{{ $vendor->city }}</td>
                <td>{{ $vendor->contact_name }} ({{ $vendor->contact_email }})</td>
                <td>                     <!-- Contracts -->
                      <form class="form-horizontal" role="form" method="GET" action="{{ url('/contract/search') }}">
                        <button type="submit" class="btn btn-link btn-sm showpayment"><i class="glyphicon glyphicon-list-alt"></i></button>
                     	{!! Form::hidden('vendor_id', $vendor->id) !!}
                     	{!! Form::hidden('searchtype', '') !!}
                      </form>
                </td>
                <td>
                     <!-- Edit -->
                     <a href="{{ URL::to('vendor/update/' . $vendor->id) }}"> <span class="glyphicon glyphicon-edit"></a>
                     <!-- Delete -->
                    <a href="{{ URL::to('vendor/delete/'. $vendor->id) }}" onclick="if(!confirm('Poistetaanko toimittaja sekä siihen liittyvät sopimukset {{$vendor->name}}?')){return false;};" title="Toimittajan poisto">&nbsp;<i class="glyphicon glyphicon-remove"></i></a>
                </td>
            </tr>
            @endforeach
             </table>
          </div>
            {!! $vendors->render() !!}
        <!--  Search section -->
        </div>

    </row>

@stop
