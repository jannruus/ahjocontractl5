@extends('layout')

@section('content')

@if ($method == 'put')  <!--  update:'put' -->
    <h4>Muuta toimittajan {!! $vendor->name !!} tietoja</h4>
@else					<!-- create:'post'-->
    <h4>Uusi toimittaja</h4>
@endif 

	{!! Form::model($vendor, array('class'=>'form-horizontal','role'=>'form','files' => true,'method' => $method,'url'=>'vendor/store/'.$vendor->id)) !!}  	<!-- $method: put=update, post=create -->

    @if ($errors->has('name'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
        {!! Form::label('name', 'Nimi',  array('class' => 'col-md-2 control-label')) !!}
       <div class="col-md-3">
            {!! Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder'=>'Nimi')) !!}
             <span class="error-msg">{!! $errors->first('name') !!}</span> 
       </div>   
    </div>
    <div class="form-group">
        {!! Form::label('address1', 'Katuosoite',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::text('address1', Input::old('address2'), array('class' => 'form-control', 'placeholder'=>'Katuosoite')) !!}
             {!! $errors->first('address1') !!}    
        </div>   
    </div>
    <div class="form-group">
        {!! Form::label('address2', 'Postiosoite', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::text('address2' , Input::old('address2'), array('class' => 'form-control')) !!}
             {!! $errors->first('address2') !!} 
        </div>    
    </div>
    <div class="form-group">
        {!! Form::label('zipcode', 'Postinumero', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::text('zipcode' , Input::old('zipcode'), array('class' => 'form-control')) !!}
            {!! $errors->first('zipcode') !!} 
        </div>    
    </div>
    
    <div class="form-group">
        {!! Form::label('city', 'Kaupunki', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::text('city' , Input::old('city'), array('class' => 'form-control')) !!}
            {!! $errors->first('city') !!} 
        </div>    
     </div>
     
     <div class="form-group">
        {!! Form::label('country', 'Maa',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::text('country', Input::old('country'), array('class' => 'form-control', 'placeholder'=>'Maa')) !!}
            {!! $errors->first('country') !!}    
        </div>   
    </div>
    
    @if ($errors->has('business_id'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
        {!! Form::label('business_id', 'LY/VAT -tunnus', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::text('business_id' , Input::old('business_id'), array('class' => 'form-control')) !!}
             <span class="error-msg">{!! $errors->first('business_id') !!}</span> 
        </div>    
    </div>
    
    @if ($errors->has('contact_name'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
        {!! Form::label('contact_name', 'Yhteyshenkilö', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::text('contact_name' , Input::old('contact_name'), array('class' => 'form-control')) !!}
             <span class="error-msg">{!! $errors->first('contact_name') !!}</span> 
        </div>    
    </div>
    
    <div class="form-group">
        {!! Form::label('contact_tel', 'Puhelinnumero', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::text('contact_tel' , Input::old('contact_tel'), array('class' => 'form-control')) !!}
            {!! $errors->first('contact_tel') !!} 
        </div>    
    </div>
    
    @if ($errors->has('contact_email'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
        {!! Form::label('contact_email', 'E-mail', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::text('contact_email', Input::old('contact_email'), array('class' => 'form-control')) !!}
            {!! $errors->first('contact_email') !!}     
        </div>    
    </div>
    <div class="form-group">
        {!! Form::label('comment', 'Lisätietoja', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::textarea('comment', Input::old('comment'), array('class' => 'form-control', 'rows' => '4')) !!}
            {!! $errors->first('comment') !!}     
        </div>    
    </div>
    
    <!--  buttons  -->
	<div class="col-md-offset-2">
    	{!! Form::submit('Talleta', array('class' => 'btn btn-primary')) !!}
        <a class="btn btn-primary" href="{!! URL::to('/vendor') !!}">Palaa</a>
    </div>     

    {!! Form::token() . Form::close() !!}

      @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-5">
        	<div class="alert alert-danger">
           		<p>{!! Session::get('error' )!!}</p>  
         	</div>
        </div>
     </row>
     @endif
     <br><br>
@stop