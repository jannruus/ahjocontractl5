@extends('layout')

	<head>
		<title>Info about AhjoContract & Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 54px;
				margin-bottom: 20px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Amazon Web Service test</div>
				
			</div>
			<br>Info about installation:
			<br>
			<table>
			<tr>
				<th style="width:200px">Muuttuja</th>
				<th style="width:200px">Arvo</th>
			</tr>
			<tr>
				<td>StoragePath</th>
				<td><?php echo storage_path();?></th>
			</tr>
			<tr>
				<td>AppPath</th>
				<td><?php echo app_path();?></th>
			</tr>
			<tr>
				<td>Url</th>
				<td><?php echo url('/');?></th>
			</tr>
			<tr>
				<td>Organisation</th>
				<td><?php echo $params->organisation(); ?></th>
			</tr>
			<tr>
				<td>Description</th>
				<td><?php echo $params->description(); ?></th>
			</tr>
			<tr>
				<td>Amazon Webservice</th>
				<td><?php echo $awsinfo; ?></th>
			</tr>
			</table>
		</div>
	</body>

