 <!--   	Payment Modal Dialog 
 		    Create or Update Payment details
 -->


<script>
$(document).ready(function(){
   $(".showpayment").click(function(e){ // Click to only happen
     
     var id = $(this).attr('data-pid');  //ajax-kutsua varten getShow
     var cid = $(this).attr('data-cid');  //contract
     $("#contract_id").val(cid);

     baseurl =  $(this).attr('data-url') +"/contract/payment/" + id; 
     //console.log( "Contract Payment JSON-call url:" + baseurl );
     
     $("#baseurl").html(baseurl);
     
   //JSON kutsu
   $.getJSON(baseurl, function(pd) {   
             $("#payment_id").val(pd.id);
             $("#type").val(pd.type);
             $("#value").val(pd.value);
             $("#comment").val(pd.comment);
          	},

            $("#payment_id").val(""),
            $("#value").val(""),
          	$("#comment").val("no comment")
            
          	 );      

     $('#showpayment').modal('show');  //näytä dialogi
   });
});
</script>


<div class="modal fade" id="showpayment" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Sopimusmaksu</h4>
      </div>
      <div class="modal-body">
		{!! 'Syötä maksun tiedot' !!}
		{!! Form::open(array('url'=>'contract/paymentstore','class'=>'form-horizontal', 'method'=> 'POST')) !!}
		    <input type="hidden" id="payment_id" name="pid" value="">
		    <input type="hidden" id="contract_id" name="cid" value="">
			<?php $types = Paymenttype::orderBy('name')->lists('name', 'id'); ?>
			
			
			{!! Form::label('type', 'Maksulaji:', array('class' => 'control-label'));!!}
			{!! Form::select('type', $types, null , array('class' => 'form-control')) !!}

			{!! Form::label('value', 'Arvo:', array('class' => 'control-label'))!!}
			<div class="input-group">
			{!! Form::text('value', Input::old('value'), array('class' => 'form-control')) !!}
			<span class="input-group-addon">€</span>
			</div>
			
			{!! Form::label('comment', 'Kommentti:', array('class' => 'control-label'))!!}
			{!! Form::textarea('comment', Input::old('comment'), array('class' => 'form-control', 'rows' => '3')) !!}

      </div>
      <div class="modal-footer">
      		{!! Form::submit('Talleta', array('class'=>'button btn btn-primary'))!!}
		{!! Form::close() !!}
        <button type="button" class="btn btn-default" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
</div>
