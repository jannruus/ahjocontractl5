
<div class="modal fade aboutModal" id="aboutModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">AhjoContract</h4>
      </div>
      <div class="modal-body">
        AhjoContract, versio 0.7. <br>
        AhjoApps 2014, 2016 <a href="http://www.ahjoapps.com">www.ahjoapps.com</a> <br><br>
        <small>Tekijät:<br>
            Janne Ruuska<br>
        </small><br>
        <?php $ccount = DB::table('contracts')->count(); ?>
        <?php $vcount = DB::table('vendors')->count(); ?>
        <?php $ucount = DB::table('users')->count(); ?>

        <small>Sopimuksia tietokannassa: <?php echo $ccount ?>
        <br>Toimittajia tietokannassa: <?php echo $vcount ?>
        <br>Käyttäjiä tietokannassa: <?php echo $ucount ?>
         <br><br>Versio: <?php  echo $params->version(); ?>
         <?php $laravel = app(); ?>
         <br>Laravel versio: <?php echo $laravel::VERSION ?>
         <br>Käyttöoikeus: <?php echo $params->organisation(); ?>
        </small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Sulje</button>
      </div>
    </div>
  </div>
</div>