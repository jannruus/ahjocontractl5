<script>
$(document).ready(function(){
   $(".showvendor").click(function(e){ // Click to only happen
     
     var id = $(this).attr('data-id');  //ajax-kutsua varten getShow
     var baseurl = window.location.host;
     baseurl = window.location.protocol + "//" + baseurl + "/vendor/showjson/" + id; 
     //document.write (baseurl); 
     
     $("#baseurl").html(baseurl);
     
    //ei json kutsu
    //$.get(baseurl, function(data) {
    //    $("#vendor-name").html(data);
    // });
     
   //JSON kutsu
    $.getJSON('vendor/showjson/' + id, function(jd) {
             $('#vendorName').html(jd.name);
             $('#vendor').html('<p> <strong>Nimi: </strong>' + jd.name + ' (id: ' + jd.id + ')' + '</p>');
             $('#vendor').append('<strong>Osoite: </strong>' + jd.address1+ ',' + jd.address2 + ',' + jd.zipcode + ' ' +  jd.city);
             $('#vendor').append('<br><strong> Business-id: </strong>' + jd.business_id );
             $('#vendor').append('<br><strong> Yhteyshenkilö: </strong>' + jd.contact_name);
             $('#vendor').append('<br><strong> E-mail: </strong>' + jd.contact_email);
             $('#vendor').append('<br><strong> Puh: </strong>' + jd.contact_tel);
             $('#vendor').append('<br> ' + jd.comment);
          });       

     $('#showvendor').modal('show');  //näytä dialogi
   });
});
</script>

<div class="modal fade" id="showvendor" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span id="vendorName"></span></h4>
      </div>
      <div class="modal-body">
        <small>
            <span id="vendor"></span><br>
            <br>[url] <span id="baseurl"></span><br>
        </small>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Sulje</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->