@extends('layout')

@section('content')

  <div class="col-md-4">
  	<br>{!! Html::image('img/Ahjoappslogo.png', 'logo') !!}<br>
    <br><br><div class="loginimg"> </div>
  </div>

{!! Form::open(array('RemindersController@postRemind', 'method' => "POST", 'class'=>'form-horizontal' , 'role'=>'form')) !!}

	<h3 class="form-signin-heading col-md-2 col-md-offset-2">Nollaa salasana</h3>

    <input type="hidden" name="token" value="{!! $token !!}">
    
    <div class="form-group">
         <div class="col-md-2 col-md-offset-6">
            {!! Form::text('email', null, array('class'=>'form-control', 'placeholder'=>'e-mail')) !!}
        </div> 
    </div>
        <div class="form-group">
         <div class="col-md-2 col-md-offset-6">
            {!! Form::text('password', null, array('class'=>'form-control', 'placeholder'=>'password')) !!}
        </div> 
    </div>
    
    <div class="form-group">
         <div class="col-md-2 col-md-offset-6">
            {!! Form::text('password_confirmation', null, array('class'=>'form-control', 'placeholder'=>'password confirmation')) !!}
        </div> 
    </div>
    
    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            {!! Form::submit('Nollaa salasana', array('class'=>'btn btn-primary'))!!}
        </div> 
    </div>


{!! Form::close() !!}

    @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-3 col-md-offset-6">
        	<div class="alert alert-danger">
           		<p>{!! Session::get('error' )!!}</p>  
         	</div>
        </div>
      </row>
    @endif

@stop