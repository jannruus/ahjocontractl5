<!DOCTYPE html>
<html>
     <head>
        <title>
            @section('title')
            AhjoContract
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

       	 <!-- Stylesheets and Fonts are placed here -->
        <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/jquery-ui.css') }}" rel="stylesheet">
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">  <!-- we use this as a bootstrap 3.3.1 style sheet -->

        <!-- Scripts are placed here -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <style>
        @section('styles')
            body {
                padding-top: 60px;
            }
        @show
        </style>
    </head>
    <body>
        <!--  read ahjocontract.json -->
        <?php $params = new App\Models\Param(); ?>
        
         <!-- Navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li><a class="navbar-brand" href="{{{ URL::to('/') }}}">AhjoContract</a></li>
                       <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sopimukset <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{{ URL::to('contract') }}}">Listaus</a></li>
                                <li><a href="{{{ URL::to('contract/new') }}}">Uusi sopimus</a></li>
                            </ul>
                        </li>
                       <li class="dropdown">      
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Toimittajat <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{{ URL::to('vendor') }}}">Listaus</a></li>
                                <li><a href="{{{ URL::to('vendor/new') }}}">Uusi toimittaja</a></li>
                            </ul>
                        </li>       
                    <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">Käyttäjät <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{{ URL::to('user/list') }}}">Listaus</a></li>
                                <li><a href="{{{ URL::to('user/register') }}}">Uusi käyttäjä</a></li>
                                <li><a href="{{{ URL::to('password/remind') }}}">Unohtuiko salasana?</a></li>
                            </ul>
                        </li>
                    <li class="dropdown">      
                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Raportit <b class="caret"></b></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{{ URL::to('/report') }}}">Sopimukset raportti</a></li>
                            </ul>
                        </li>           
                    <li>@if (Auth::check())
                             <a href="{{{ URL::to('user/logout') }}}">Kirjaudu ulos</a>
                        @else
                            <a href="{{{ URL::to('auth/login') }}}">Kirjaudu sisään</a>
                        @endif 
                    </li>
                    <li>
                   <!-- Button trigger modal -->

                         <a class="btn btn-link" data-toggle="modal" data-target="#aboutModal">Tietoja</a>
                    </li>   
                </ul>
            </div>
            
        </div> 

        <!-- Container -->
        <div class="container-fluid">
            @include('about')   
            @yield('content')<br>
            
            @if(Session::has('message'))
            <row>
            <div class="col-md-5">
                <div class="alert alert-success">
                  <p>{{ Session::get('message' )}}</p>  
                </div></div>
            @endif
            
            </row>    
        </div>

    </body>
</html>