<!-- Show Contract view -->
@extends('layout')
@section('content')
@include('showpayment')  


@if ($mode == 'U')
    <h4>Muuta sopimusta {!! $contract->name !!}</h4>
    {!! Form::model($contract, array('class'=>'form-horizontal','role'=>'form','files' => true,'method' => 'post','url'=>'contract/store')) !!}
    {!! Form::hidden('id', $contract->id) !!}
@else
    <h4>Uusi sopimus</h4>
    {!! Form::open(array('class'=>'form-horizontal' , 'role'=>'form','files' => true, 'method' => 'post','url'=>'contract/store')) !!}
    {!! Form::hidden('id', '0') !!}
@endif 

    @if ($errors->has('name'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
        {!! Form::label('name', 'Sopimus',  array('class' => 'col-md-2 control-label')) !!}
       <div class="col-md-4">
            {!! Form::text('name', Input::old('name'), array('class' => 'form-control', 'placeholder'=>'sopimuksen nimi')); !!}
            <span class="error-msg">{!!$errors->first('name')!!}</span> 
       </div>   
    </div>

    <div class="form-group">
        {!! Form::label('vendor', 'Toimittaja',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-4">    
            <?php $vendors = Vendor::orderBy('name')->lists('name', 'id'); ?>
            @if ($mode == 'U')
                {!! Form::select('vendor', $vendors, $contract->vendor_id , array('class' => 'form-control')) !!}
            @else
                {!! Form::select('vendor', $vendors,null, array('class' => 'form-control')) !!}
            @endif    
            {!! $errors->first('vendor') !!}      
        </div>   
    </div>
    
    <div class="form-group">
        {!! Form::label('type', 'Sopimuslaji',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-4">    
            <?php $types = Contracttype::orderBy('name')->lists('name', 'id') ?>
            @if ($mode == 'U')
                  {!! Form::select('type', $types, $contract->type , array('class' => 'form-control')) !!}
            @else
                  {!! Form::select('type', $types, null , array('class' => 'form-control')) !!}
            @endif
            {!! $errors->first('type') !!}      
        </div>   
    </div>
    
    @if ($errors->has('start_date'))	
    <div class="form-group has-error">
    @else
 	<div class="form-group">
    @endif   
        {!! Form::label('start_date', 'Alkupäivä', array('class' => 'col-md-2 control-label')) !!}
            <div class=" col-md-2">
                <div class="input-group">
                    @if ($mode == 'U')
                        <input id="start_date" name="start_date" type="text" value="<?php  echo date("d.m.Y", strtotime($contract->start_date)) ?>"  class="date-picker form-control" />
                    @else
                        <input id="start_date" name="start_date" type="text" class="date-picker form-control" />
                    @endif
                    <label for="start_date" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                    </label>
                        <script>
                            $(function() {
                            $( "#start_date" ).datepicker({
                            dateFormat: 'dd.mm.yy',
                            changeMonth: true,
                            changeYear: true,
                            calendarWeeks: true,
                            beforeShow: function(){ $(".ui-datepicker").css('font-size', 12) }
                            });
                            });
                    </script>
            </div>
       </div>
    </div>
    
    @if ($errors->has('end_date'))	
    <div class="form-group has-error">
    @else
 	<div class="form-group">
    @endif   
        {!! Form::label('end_date', 'Loppupäivä', array('class' => 'col-md-2 control-label')) !!}
            <div class=" col-md-2">
                <div class="input-group">
                    @if ($mode == 'U')
                        <input id="end_date" name="end_date" type="text" value="<?php  
                        	if ($contract->end_date != ''){echo date("d.m.Y", strtotime($contract->end_date)); }
                        		else{echo '';}    
                        	?>" 
                        	class="date-picker form-control" />
                    @else
                         <input id="end_date" name="end_date" type="text" class="date-picker form-control" />
                    @endif
                    <label for="end_date" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                    </label>
                        <script>
                            $(function() {
                            $( "#end_date" ).datepicker({
                            dateFormat: 'dd.mm.yy',
                            changeMonth: true,
                            changeYear: true,
                            beforeShow: function(){ $(".ui-datepicker").css('font-size', 12) }
                            });
                            });
                    </script>
            </div>
       </div>
    </div>
    
    <!-- 
    <div class="form-group form-group-sm">
       {!! Form::label('continuous', ' ', array('class' => 'col-md-2 control-label'));!!}
        <div class="col-md-offset-2">
            <div class="checkbox">
            {!! Form::checkbox('continuous')!!} Jatkuu sopimuskauden jälkeen toistaiseksi    
            </div>
             {!! $errors->first('continous') !!} 
        </div>    
    </div>
	-->

        <div class="form-group">
        	<div class="col-md-4 col-md-offset-2">
            	<div class="">
            		{!! Form::checkbox('countinuous')!!}  Jatkuu sopimuskauden jälkeen toistaiseksi    
            	</div>
             	{!! $errors->first('countinuous') !!} 
        	</div>
        </div>
            
   @if ($errors->has('termination_period'))	
   	<div class="form-group has-error">
   @else
      	<div class="form-group">
   @endif	
        {!! Form::label('termination_period', 'Irtisanomisaika', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-2">
            <div class="input-group">   
            	{!! Form::text('termination_period' , Input::old('termination_period'), array('class' => 'form-control')) !!}
            <span class="input-group-addon">kk</span>
            </div>
            <span class="error-msg">{!!$errors->first('termination_period')!!}</span> 
        </div>    
    </div>	

    
  <!--    Value removed
    <div class="form-group form-group-sm">
        {!! Form::label('value', 'Arvo', array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-4">
            <div class="input-group">
            {!! Form::text('value' , Input::old('value'), array('class' => 'form-control')) !!}
            <span class="input-group-addon">€</span>
            </div>
             {!! $errors->first('value') !!} 
        </div>    
    </div>
    -->
    
     <div class="form-group">
        {!! Form::label('value', 'Maksut', array('class' => 'col-md-2 control-label'))!!}
    	<div class="col-md-4">
    	@if (isset($payments))
    	<table class="table-condensed table-striped input-sm">
    	<tr>
    		<th style="width:150px">Laji</th>
    		<th style="width:110px">Maksu</th>
    		<th style="width:70px"></th>
    	</tr>
    	@foreach($payments as $payment)
    	<tr>
    		<td>{!! $payment->typename() !!}</td>
    		<td> {!! $payment->value !!}€</td> 
    		<td>
    			<a class="btn btn-link btn-sm showpayment" data-target="#showpayment" data-pid="{!!$payment->id!!}"  data-url="{!! url('/') !!}" > <span class="glyphicon glyphicon-edit"></span></a>
            	<a href="{!! URL::to('contract/paymentdelete/' . $payment->id) !!}"> <span class="glyphicon glyphicon-remove"></span></a>
            </td>
           <!--   {!! $payment->comment !!} -->
    	</tr>
    	@endforeach
    	</table>
    	<a class="btn btn-link btn-sm showpayment" data-target="#showpayment" data-cid="{!!$contract->id!!}" data-pid="0"> <span class="glyphicon glyphicon-plus-sign"></span> Lisää uusi</a>
    	@else
    		<span class="glyphicon glyphicon-plus-sign"></span> Lisää uusi
    	@endif	
    	</div>
    </div>	
    
    
    <!-- List of Contract files -->
    <?php $storagedir =  App\Filestorage::path(); ?>
    <div class="form-group">
        {!! Form::label('files', 'Sopimusdokumentit', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-4">
        @if (isset($files))
       		<table class="table-condensed table-striped input-sm">
    		<tr>
    			<th style="width:400px">Tiedosto (max koko: <?php echo ini_get('upload_max_filesize') ?>)</th>
    			<th style="width:70px"></th>
    		</tr>
    		@foreach($files as $file)
    		<tr>
    			<td>	
    				<span class="glyphicon glyphicon-download-alt">&nbsp;</span>{!! link_to('/download/' . $file->filename , $file->org_filename) !!}
    			</td>
    			<td>
            		<a href="{!! URL::to('contract/filedelete' .'/' . $file->id) !!}">  <span class="glyphicon glyphicon-remove"></span></a>
            	</td>
    		</tr>
    		@endforeach
    		</table>
    	@else
    	@endif
    	{!! Form::file('contract[]',  array('multiple'=>true)) !!}
        </div>    
    </div>
    
    
    <div class="form-group">
        {!! Form::label('filelink', 'Sopimuslinkki', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-4">
            {!! Form::text('filelink', Input::old('filelink'), array('class' => 'form-control')) !!}
            {!! $errors->first('filelink') !!}     
        </div>    
    </div>

   <div class="form-group">
        {!! Form::label('status', 'Sopimuksen tila',  array('class' => 'col-md-2 control-label')); !!}
        <div class="col-md-4">    
            <?php $statuss = Status::orderBy('name')->lists('name', 'id'); ?>
             @if ($mode == 'U')
                 {!! Form::select('status', $statuss, $contract->status, array('class' => 'form-control')) !!}
             @else
                {!! Form::select('status', $statuss,null, array('class' => 'form-control'),'V') !!}
            @endif    
            {!! $errors->first('status') !!}      
        </div>   
    </div>

    <div class="form-group">
        {!! Form::label('comment', 'Lisätietoja', array('class' => 'col-md-2 control-label')); !!}
        <div class="col-md-4">
            {!! Form::textarea('comment', Input::old('comment'), array('class' => "form-control", 'rows' => "5")) !!}
            {!! $errors->first('comment') !!} 
               
            @if ($mode == 'U')
            	<?php
            	try {
            		$user_name = User::find($contract->user_id)->username;	            		
            	}catch(Exception $e){
            		$user_name = 'no_name';
            	}
            	?> 

               <br><span style="color:grey;font-size:12px"><i> {!! 'Muokattu ' . $user_name . '->'. $contract->updated_at !!}</i></span>
            @endif
        </div>
   </div>
    
   <!--  buttons --> 
   <div class="col-md-offset-2">
            {!! Form::submit('Talleta', array('class' => 'btn btn-primary')) !!}
            <a class="btn btn-primary" href="{!! URL::to('/') !!}">Palaa</a>
   </div>

    {!! Form::token() . Form::close() !!}

     @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-5">
        	<div class="alert alert-danger">
           		<p>{!! Session::get('error' )!!}</p>  
         	</div>
        </div>
     </row>
     @endif
    
    <br><br>
    
@stop