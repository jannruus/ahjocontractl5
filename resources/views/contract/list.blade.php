<!-- Contract list view -->
@extends('layout')
@section('content')
@include('showvendor')

     <row>
         <div class="col-md-4 col-md-offset-2">
                <button type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-plus"></span> <a href="{{ URL::to('contract/new') }}">Luo uusi</a>
                </button>

       <!-- Buttongroup: Contracts/Notes issue#10-->

		<div class="btn-group" role="group" aria-label="...">
			@if(Session::has('type'))
				@if(Session::get('type') === 'N')   <!--   Notes selected -->
					<a href="{{ url('/contract/type/Z') }}" class="btn btn-default btn-sm" role="button">Sopimukset</a>
					<a href="{{ url('/contract/type/N') }}" class="btn btn-default btn-sm active" role="button">Muistiot</a>
					<?php $types = Contracttype::orderBy('name')->where('id','N')->lists('name', 'id')->all(); ?>
					<?php  Session::flash('searchtype', 'N'); ?>
				@else
					<a href="{{ url('/contract/type/Z') }}" class="btn btn-default btn-sm active" role="button">Sopimukset</a>
					<a href="{{ url('/contract/type/N') }}" class="btn btn-default btn-sm" role="button">Muistiot</a>
					<?php $types = array(''=> "ei valittu ") + Contracttype::orderBy('name')->where('id','<>','N')->lists('name', 'id')->all(); ?>
				@endif

			@endif

		</div>

        </div>
    </row>

    <br><br>

    <row>

    	<!-- Search section -->
        <div class="col-md-2">
        <div class="panel panel-default bs-search">
              <div class="form-group">
                <form class="form-horizontal" role="form" method="GET" action="{{ url('/contract/search') }}">

                	<label for="haku" class="control-label input-sm">Nimihaku</label>
                    {!!Form::text('keyword', Session::get('keyword'), array('class' => 'form-control input-sm', 'placeholder'=>'etsi nimellä'))!!}

                    <label for="searchtype" class="control-label input-sm">Laji</label>

                    @if(Session::has('searchtype'))
                        {!! Form::select('searchtype', $types, Session::get('searchtype') , array('class' => 'form-control input-sm')) !!}
                    @else
                        {!! Form::select('searchtype', $types, '' , array('class' => 'form-control input-sm'),'') !!}
                    @endif

                    <label for="vendor" class="control-label input-sm">Toimittaja</label>
                    <?php $vendors = array(''=> "ei valittu ") + Vendor::orderBy('name')->lists('name', 'id')->all(); ?>
                    {!! Form::select('vendor_id', $vendors, Session::get('vendor_id') , array('class' => 'form-control input-sm'),'') !!} <br>

              		<button type="submit" class="btn btn-primary btn-sm">Etsi</button>
              		<a href="{{ url('/contract/clear/') }}" class="btn btn-primary btn-sm" role="button">Tyhjennä</a>

              </form> </div> </div>
          </div>

		<!-- List section -->

        <div class="col-md-10">

        <div class="panel panel-primary">
                  <!--  <div class="panel panel-primary" style="width:800px"> -->
  		<!-- Default panel contents -->
            <div class="panel-heading">Sopimukset</div>

    	<!-- Table -->
            <table class="table-condensed table-striped table-hover input-sm">
            <tr>
                <th style="width:300px">
                @if (Session::get('sortby') == 'name' && Session::get('order') == 'asc') {!!

                   link_to_action(
                            'App\Http\Controllers\ContractController@getIndex',
                            'Nimi',
                            array(
                                'sortby' => 'name',
                                'order' => 'desc'
                            )
                        )
                    !!}
                    @else {!!
                        link_to_action(
                            'App\Http\Controllers\ContractController@getIndex',
                            'Nimi',
                            array(
                                'sortby' => 'name',
                                'order' => 'asc'
                            )
                        )
                    !!}
                    @endif
                </th>
                <!-- <th style="width:300px">Toimittaja</th>  -->
                <th style="width:300px">
                @if (Session::get('sortby') == 'vendor_id' && Session::get('order') == 'asc') {!!

                        link_to_action('App\Http\Controllers\ContractController@getIndex',
                            'Toimittaja',
                            array(
                                'sortby' => 'vendor_id',
                                'order' => 'desc'
                            )
                        )
                    !!}
                    @else {!!
                        link_to_action('App\Http\Controllers\ContractController@getIndex',
                            'Toimittaja',
                            array(
                                'sortby' => 'vendor_id',
                                'order' => 'asc'
                            )
                        )
                    !!}
                    @endif
                    </th>
                <th style="width:150px">Laji</th>
                <th style="width:110px">Alkupvm</th>
                <th style="width:110px">Loppupvm</th>
                <th style="width:55px">Action</th>
            </tr>

            @foreach($contracts as $contract)
            <tr>
                <td> <a href="{{ URL::to('contract/update/' . $contract->id) }}"> {{ $contract->name }}</a></td>
                <td><a class="btn btn-link btn-sm showvendor" data-target="#showvendor" data-id="{{$contract->vendor_id}}" > {{ $contract->vendorname()->name }}</a></td>
               <!-- <td><a href="{{ URL::to('vendor/update/' . $contract->vendor_id) }}"> {{ $contract->vendorname()->name  }}</a></td> -->
                <td>{{ $contract->typename() }}</td>
                <td><?php  echo date("d.m.Y", strtotime($contract->start_date)) ?> </td>
                <td><?php
                		if ($contract->end_date != ''){echo date("d.m.Y", strtotime($contract->end_date)); }
                		else{echo '';}
                	?></td>
                <td>
                        @if($contract->filelink != '')
                            <a href="{{ $contract->filelink }}"><span class="glyphicon glyphicon-folder-open"></span></a>
                        @else
                            <span class="glyphicon glyphicon-folder-close"></span>
                        @endif


                	<a href="{{ URL::to('contract/delete/'. $contract->id) }}" onclick="if(!confirm('Poistetaanko sopimus {{$contract->name}}?')){return false;};" title="Sopimuksen poisto">&nbsp;<i class="glyphicon glyphicon-remove"></i></a>
            	</td>
            </tr>
            @endforeach
             </table>

    	</div>
          {!! $contracts->appends(array('sortby' => Session::get('sortby'), 'order' => Session::get('order')))->render() !!}

   		</div>


   </row>

@stop
