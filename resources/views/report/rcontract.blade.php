@extends('layout')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-6 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Sopimukset -raportti</div>
					<div class="panel-body">
						<form class="form-horizontal" role="form" method="get" action="{{ url('/report/pdf') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

                    		{!! Form::label('vendor', 'Toimittaja',  array('class' => 'control-label')) !!}
                    		<?php $vendors = array(''=> "kaikki ") + Vendor::orderBy('name')->lists('name', 'id')->all(); ?>
                    		{!! Form::select('vendor_id', $vendors, Session::get('vendor_id') , array('class' => 'form-control input-sm'),'') !!} <br>
                     		<button type="submit" class="btn btn-primary btn-sm">Tulosta</button>                
              			</form>
              		</div>  						
			</div>
		</div>
	</div>
</div>
@endsection
