<!DOCTYPE html>
<html>
<!-- Contracts by vendors pdf-report, no Notes -->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<style type="text/css">
@page { margin: 60px 50px; }
.header { position: fixed; left: 0px; top: -30px; right: 0px; height: 50px;}
.header .page:after { content: counter(page); }
th, td { table-layout:fixed; text-align: left; }
th { border-bottom: 1px solid; }
	
</style>
</head>

<body>
   <div class="header">
   	<table width="100%">
   		<tr>	
     		<td><span>AhjoContract -sopimukset</span></td>
     		<td><?php 
				$thedate = date("d.m.Y"); 
				print $thedate; 
			?></td>
     		<td style="text-align:right;"><span class="page">Sivu <?php $PAGE_NUM ?></span></td>
     	</tr>
     </table>
     <hr>
   </div>

<?php
	if ($vendor_id == '') {
		$vendors = Vendor::orderBy('name', 'ASC')->get();
	} else {
		$vendors = Vendor::where('id' , '=' , $vendor_id)->get();
	}
?>
<br><br>          
            <table>
            	<tr>
            		<th style="width:80mm;">Toimittajan sopimukset</th>
            		<th style="width:30mm;">Laji</th>
            		<th>Alkupvm</th>
            		<th>Loppupvm</th>
          		</tr>
          		
          		 @foreach($vendors as $vendor)	 	
            	<tr>
            		<td>
               			<b>{{ $vendor->name }}</b>
               		</td>
               	</tr>
               	
            	<?php $contracts = Contract::where('vendor_id' , '=' , $vendor->id)->where('type' , '<>' , 'N')->get();  ?>
            	@foreach($contracts as $contract)
            	 <tr>
            	 	<td>{{ $contract->name }}</td>
            	 	<td>{{ $contract->typename() }}</td>
            	 	<td>&nbsp;<?php  echo date("d.m.Y", strtotime($contract->start_date)) ?></td>
            	 	<td>&nbsp;<?php
                		if ($contract->end_date != ''){echo date("d.m.Y", strtotime($contract->end_date)); }
                		else{echo '-';}
                	?></td>
            	 </tr>
            	 @endforeach
            	 <tr><td>Sopimuksia: <?php echo count($contracts); ?></td></tr>
            	
            	 @endforeach
            </table>
                        
</body>
</html>