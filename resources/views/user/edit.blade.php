@extends('layout')

@section('content')


    <h2 class="form-signup-heading">Päivitä käyttäjätiedot</h2>
    {!! Form::model($user, array('class'=>'form-horizontal', 'method' => 'put', 'action' => array('App\Http\Controllers\UserController@putUpdate',$user->id) ) ) !!}
 
    <div class="form-group">
        {!! Form::label('username', 'Käyttäjätunnus',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::label('username', $user->username,  array('class' => 'control-label')) !!}
        </div> 
    </div>
    
    @if ($errors->has('email'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif 
        {!! Form::label('email', 'Sähköposti',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">   
            {!! Form::text('email', Input::old('email'), array('class' => 'form-control','placeholder'=>'Email osoite')) !!}
            {!! $errors->first('email') !!}  
        </div> 
    </div>
    
    <div class="form-group">
        {!! Form::label('chg_pwd', 'Päivitä salasana',  array('class' => 'col-sm-2 control-label'))!!}
        <div class="col-sm-2">  
            {!! Form::checkbox('chg_pwd', 'yes', false) !!}
            {!! Form::label('chg_pwd', 'Kyllä'); !!} 
        </div>    
    </div>
    
    @if ($errors->has('password'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif    
        {!! Form::label('password', 'Salasana',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-3">
            {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Salasana')) !!}
             <span class="error-msg">{!! $errors->first('password') !!}</span> 
        </div> 
     </div>
     
    @if ($errors->has('password_confirmation'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif 
        {!! Form::label('password_confirmation', 'Vahvista salasana',  array('class' => 'col-md-2 control-label'))!!}
        <div class="col-md-3">
            {!! Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Vahvista salasana')) !!}
             <span class="error-msg">{!! $errors->first('password_confirmation') !!}</span> 
        </div> 
     </div>
     
    <div class="form-group">
        {!! Form::label('is_admin', 'Admin käyttäjä',  array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-2">  
            {!! Form::checkbox('is_admin') !!}
            {!! Form::label('is_admin', ' Kyllä')!!} 
        </div>    
    </div>
 
    <!--  buttons  -->
	<div class="col-md-offset-2">
    	{!! Form::submit('Talleta', array('class'=>'btn btn-primary'))!!}
    	<a class="btn btn-primary" href="{!! URL::to('user/list') !!}">Palaa</a>
    </div>
    
    
 	{!! Form::token() . Form::close() !!}
 
    @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-5">
        	<div class="alert alert-danger">
           		<p>{!! Session::get('error' ) !!}</p>  
         	</div>
        </div>
      </row>
    @endif

     
@stop