@extends('layout')
@section('content')

    <row>
    <div class="col-md-8 col-md-offset-2">
    	<button type="button" class="btn btn-default btn-sm">
        	<span class="glyphicon glyphicon-user"></span> <a href="./register">Uusi käyttäjä</a>
    	</button>
    </div> 
    </row>
    
    <br><br>
    
    <row>
    <div class="col-md-10 col-md-offset-2">
    
    <div class="panel panel-primary" style="width:600px">
  <!-- Default panel contents -->
        <div class="panel-heading">Käyttäjät</div>

    <!-- Table -->
            <table class="table-condensed table-striped table-hover">
            <tr>
                <th style="width:200px">Käyttäjätunnus</th>
                <th style="width:300px">Sähköposti</th> 
                <th style="width:100px"> </th> 
            </tr>
            @foreach($users as $user)
            <tr>
                <td> <a href="{!! URL::to('user/update/' . $user->id) !!}"> {!! $user->username !!}</a></td>
                <td>{!! $user->email !!}</td>
                <td>
                    {!! Form::open(array('class' => 'pull-right', 'method' => 'put', 'action' => array('App\Http\Controllers\UserController@putDelete',$user->id))) !!}
                       <!--  {!! Form::submit('Poista ', array('class' => 'btn btn-xs btn-warning')) !!} -->
                     <button class='btn btn-link btn-xs' type='submit'> 
                         <span class="glyphicon glyphicon-remove"></span>
                     </button>
                       
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
             </table>
            {!! $users->render() !!}
    </div>
    </div>
    </row>
        
@stop