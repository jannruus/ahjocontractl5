@extends('layout')

@section('content')

{!! Form::open(array('class'=>'form-signin form-horizontal','role'=>'form', 'url'=>'user/create')) !!}
    <h2 class="form-signup-heading">Käyttäjän lisäys</h2>
 
    @if ($errors->has('username'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
   	{!! Form::label('username', 'Käyttäjätunnus',  array('class' => 'col-md-2 control-label')) !!}
    <div class="col-md-3">
        {!! Form::text('username', null , array('class' => 'form-control','placeholder'=>'Käyttäjätunnus')) !!}
        <span class="error-msg">{!! $errors->first('username') !!}</span> 
    </div> 
    </div>
    
    @if ($errors->has('email'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
    {!! Form::label('email', 'Sähköpostiosoite',  array('class' => 'col-md-2 control-label')) !!}
    <div class="col-md-3">   
        {!! Form::text('email', null, array('class' => 'form-control','placeholder'=>'Email osoite')) !!}
        <span class="error-msg">{!! $errors->first('email') !!}</span> 
    </div> 
    </div>
    
    @if ($errors->has('password'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif 
     {!! Form::label('password', 'Salasana',  array('class' => 'col-md-2 control-label')) !!}
    <div class="col-md-3">
        {!! Form::password('password', array('class' => 'form-control','placeholder'=>'Salasana')) !!}
        <span class="error-msg">{!! $errors->first('password') !!}</span> 
     </div> 
     </div>
     
    @if ($errors->has('password_confirmation'))	
    <div class="form-group has-error">
    @else
    <div class="form-group">
    @endif
     {!! Form::label('password_confirmation', 'Vahvista salasana',  array('class' => 'col-md-2 control-label')) !!}
     <div class="col-md-3">
        {!! Form::password('password_confirmation', array('class' => 'form-control','placeholder'=>'Vahvista salasana')) !!}
        <span class="error-msg">{!! $errors->first('password_confirmation') !!}</span> 
     </div> 
     </div>
     
     <div class="form-group">
        {!! Form::label('is_admin', 'Admin käyttäjä',  array('class' => 'col-sm-2 control-label')) !!}
        <div class="col-sm-2">  
            {!! Form::checkbox('is_admin') !!}
            {!! Form::label('is_admin', ' Kyllä') !!} 
        </div>    
    </div>
 
    <!--  buttons  -->
	<div class="col-md-offset-2">
        {!! Form::submit('Talleta', array('class'=>'btn btn-large btn-primary'))!!}
        <a class="btn btn-primary" href="{!! URL::to('user/list') !!}">Palaa</a>
    </div>
        
 	{!! Form::token() . Form::close() !!}
 
    @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-5">
        	<div class="alert alert-danger">
           		<p>{!! Session::get('error' )!!}</p>  
         	</div>
        </div>
      </row>
    @endif
 
@stop