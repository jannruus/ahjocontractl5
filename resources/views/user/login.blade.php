@extends('layout')

@section('content')

  <div class="col-md-4">
  	<br>{{ HTML::image('img/Ahjoappslogo.png', 'logo') }}<br>
    <br><br><div class="loginimg"> </div>
  </div>
  
{{ Form::open(array('url'=>'user/signin', 'class'=>'form-horizontal' , 'role'=>'form')) }}

    
    <h3 class="form-signin-heading col-md-2 col-md-offset-2">Kirjaudu</h3>
    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            {{ Form::text('username', null, array('class'=>'form-control', 'placeholder'=>'Käyttäjätunnus')) }}
        </div> 
    </div>

    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            {{ Form::password('password', array('class'=>'form-control', 'placeholder'=>'Salasana')) }}
        </div> 
    </div>
    <br>
    <div class="form-group">
        <div class="col-md-2 col-md-offset-6">
            {{ Form::submit('Kirjaudu sisään', array('class'=>'btn btn-primary'))}}
        </div> 
    </div>
{{ Form::close() }}
   
    @if(Session::has('error'))
      <br>
      <row>
      	<div class="col-md-3 col-md-offset-6">
        	<div class="alert alert-danger">
           		<p>{{ Session::get('error' )}}</p>  
         	</div>
        </div>
      </row>
    @endif

@stop